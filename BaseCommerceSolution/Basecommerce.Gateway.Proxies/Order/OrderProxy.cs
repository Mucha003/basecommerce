﻿using Basecommerce.Common;
using Basecommerce.Common.Extentions;
using Basecommerce.Gateway.Models;
using Basecommerce.Gateway.Models.Order;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Basecommerce.Gateway.Proxies.Order
{
    public class OrderProxy : IOrderProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public OrderProxy(HttpClient httpClient, IOptions<ApiUrls> apiUrls, IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }


        public async Task<DataCollection<OrderResponse>> GetAllAsync(int page, int take)
        {
            var fullUrl = $"{_apiUrls.OrderUrl}/api/Orders?page={page}&take={take}";
            HttpResponseMessage request = await _httpClient.GetAsync(fullUrl);
            request.EnsureSuccessStatusCode();

            var result = JsonSerializer.Deserialize<DataCollection<OrderResponse>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );

            return result;
        }



        public async Task<OrderResponse> GetAsync(int id)
        {
            HttpResponseMessage request = await _httpClient.GetAsync($"{_apiUrls.OrderUrl}/api/Orders/{id}");
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<OrderResponse>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }



        public async Task CreateAsync(OrderCreateDto command)
        {
            StringContent content = new StringContent(
                JsonSerializer.Serialize(command),
                Encoding.UTF8,
                "application/json"
            );

            HttpResponseMessage request = await _httpClient.PostAsync($"{_apiUrls.OrderUrl}/api/Orders", content);
            request.EnsureSuccessStatusCode();
        }


    }
}
