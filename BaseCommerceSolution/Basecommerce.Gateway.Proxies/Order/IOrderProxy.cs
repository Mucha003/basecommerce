﻿using Basecommerce.Common;
using Basecommerce.Gateway.Models;
using Basecommerce.Gateway.Models.Order;
using System.Threading.Tasks;

namespace Basecommerce.Gateway.Proxies.Order
{
    public interface IOrderProxy
    {
        Task<DataCollection<OrderResponse>> GetAllAsync(int page, int take);
        Task<OrderResponse> GetAsync(int id);
        Task CreateAsync(OrderCreateDto command);
    }
}
