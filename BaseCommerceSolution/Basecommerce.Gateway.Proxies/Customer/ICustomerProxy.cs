﻿using Basecommerce.Common;
using Basecommerce.Gateway.Models.Customers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Basecommerce.Gateway.Proxies.Customer
{
    public interface ICustomerProxy
    {
        Task<DataCollection<ClientDto>> GetAllAsync(int page, int take, IEnumerable<int> clients = null);
        Task<ClientDto> GetAsync(int id);
    }
}
