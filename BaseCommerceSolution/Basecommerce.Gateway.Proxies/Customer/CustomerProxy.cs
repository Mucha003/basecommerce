﻿using Basecommerce.Common;
using Basecommerce.Common.Extentions;
using Basecommerce.Gateway.Models;
using Basecommerce.Gateway.Models.Customers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace Basecommerce.Gateway.Proxies.Customer
{
    public class CustomerProxy : ICustomerProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;

        public CustomerProxy(HttpClient httpClient, IOptions<ApiUrls> apiUrls, IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);

            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }


        public async Task<DataCollection<ClientDto>> GetAllAsync(int page, int take, IEnumerable<int> clients = null)
        {
            string ids = string.Join(',', clients ?? new List<int>());
            string fullUrl = $"{_apiUrls.CustomerUrl}/api/Clients?page={page}&take={take}&ids={ids}";

            HttpResponseMessage request = await _httpClient.GetAsync(fullUrl);
            request.EnsureSuccessStatusCode();

            var result = JsonSerializer.Deserialize<DataCollection<ClientDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );

            return result;
        }



        public async Task<ClientDto> GetAsync(int id)
        {
            HttpResponseMessage request = await _httpClient.GetAsync($"{_apiUrls.CustomerUrl}/api/Clients/{id}");
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<ClientDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }


    }
}
