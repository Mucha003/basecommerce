﻿using Basecommerce.Common;
using Basecommerce.Gateway.Models.Catatlog;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Basecommerce.Gateway.Proxies.Catalog
{
    public interface ICatalogProxy
    {
        Task<DataCollection<ProductDto>> GetAllAsync(int page, int take, IEnumerable<int> clients = null);
        Task<ProductDto> GetAsync(int id);
    }
}
