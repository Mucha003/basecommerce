﻿using Basecommerce.Common;
using Basecommerce.Common.Extentions;
using Basecommerce.Gateway.Models;
using Basecommerce.Gateway.Models.Catatlog;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace Basecommerce.Gateway.Proxies.Catalog
{
    public class CatalogProxy : ICatalogProxy
    {
        private readonly ApiUrls _apiUrls;
        private readonly HttpClient _httpClient;


        public CatalogProxy(HttpClient httpClient, IOptions<ApiUrls> apiUrls, IHttpContextAccessor httpContextAccessor)
        {
            httpClient.AddBearerToken(httpContextAccessor);
            _httpClient = httpClient;
            _apiUrls = apiUrls.Value;
        }


        public async Task<DataCollection<ProductDto>> GetAllAsync(int page, int take, IEnumerable<int> Products = null)
        {
            var ids = string.Join(',', Products ?? new List<int>());

            var request = await _httpClient.GetAsync($"{_apiUrls.CatalogUrl}/products?page={page}&take={take}&ids={ids}");
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<DataCollection<ProductDto>>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }



        public async Task<ProductDto> GetAsync(int id)
        {

            var request = await _httpClient.GetAsync($"{_apiUrls.CatalogUrl}/Products/{id}");
            request.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<ProductDto>(
                await request.Content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );
        }


    }
}
