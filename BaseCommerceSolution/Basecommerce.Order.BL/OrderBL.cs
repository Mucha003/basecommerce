﻿using Basecommerce.Common;
using Basecommerce.Order.BL.Repository;
using Basecommerce.Order.DAL;
using Basecommerce.Order.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Basecommerce.Order.BL
{
    public class OrderBL : IOrderRepos
    {
        private readonly OrderContext _context;

        public OrderBL(OrderContext context)
        {
            _context = context;
        }


        public async Task<DataCollection<OrderDto>> GetAllAsync(int page, int take)
        {
            DataCollection<DAL.Order> collection = await _context.Orders
                .Include(x => x.Items)
                .OrderByDescending(x => x.OrderId)
                .GetPagedAsync(page, take);

            return collection.MapTo<DataCollection<OrderDto>>();
        }



        public async Task<OrderDto> GetAsync(int id)
        {
            return (await _context.Orders
                                  .Include(x => x.Items)
                                  .SingleAsync(x => x.OrderId == id)
                   ).MapTo<OrderDto>();
        }

    }
}
