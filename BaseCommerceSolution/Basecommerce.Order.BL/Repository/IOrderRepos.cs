﻿using Basecommerce.Common;
using Basecommerce.Order.Models;
using System.Threading.Tasks;

namespace Basecommerce.Order.BL.Repository
{
    public interface IOrderRepos
    {
        Task<DataCollection<OrderDto>> GetAllAsync(int page, int take);
        Task<OrderDto> GetAsync(int id);
    }
}
