﻿using System.Collections.Generic;

namespace Basecommerce.Order.Models
{
    public class StockUpdate
    {
        public IEnumerable<StockUpdateItem> Items { get; set; } = new List<StockUpdateItem>();
    }
}
