﻿using Microsoft.AspNetCore.Http;
using System.Net.Http;

namespace Basecommerce.Order.Models.Extentions
{
    public static class HttpClientTokenExtention
    {
        // IHttpContextAccessor == permet d'acceder au request Actuelle et lire les info du request.
        public static void AddBearerToken(this HttpClient client, IHttpContextAccessor context)
        {
            // HttpContext.Request.Headers == header mis dans postman pour l'acces Token (Authorization  =  Bearer ........)
            if (context.HttpContext.User.Identity.IsAuthenticated && context.HttpContext.Request.Headers.ContainsKey("Authorization"))
            {
                string token = context.HttpContext.Request.Headers["Authorization"].ToString();

                if (!string.IsNullOrEmpty(token))
                {
                    // ce token on l'inject a l'httpClient.
                    // check CatalogHttpProxy 
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", token);

                    // Ajouter le service dans le startup : services.AddHttpContextAccessor();
                }
            }
        }
    }
}
