﻿using Basecommerce.Order.Models.Enums;
using System;
using System.Collections.Generic;

namespace Basecommerce.Order.Models
{
    public class OrderDto
    {
        public int OrderId { get; set; }
        public string OrderNumber => CreatedAt.Year + "-" + OrderId.ToString().PadLeft(6, '0');
        public OrderStatus Status { get; set; }
        public OrderPayment PaymentType { get; set; }
        public int ClientId { get; set; }
        public IEnumerable<OrderDetailDto> Items { get; set; } = new List<OrderDetailDto>();
        public DateTime CreatedAt { get; set; }
        public decimal Total { get; set; }
    }
}
