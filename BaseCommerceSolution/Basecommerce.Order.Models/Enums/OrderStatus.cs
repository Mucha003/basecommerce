﻿namespace Basecommerce.Order.Models.Enums
{
    public enum OrderStatus
    {
        Cancel,
        Pending,
        Approved
    }
}
