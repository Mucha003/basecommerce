﻿namespace Basecommerce.Order.Models.Enums
{
    public enum OrderPayment
    {
        CreditCard,
        PayPal,
        BankTransfer
    }
}
