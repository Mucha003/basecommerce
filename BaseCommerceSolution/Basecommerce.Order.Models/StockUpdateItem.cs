﻿using Basecommerce.Order.Models.Enums;

namespace Basecommerce.Order.Models
{
    public class StockUpdateItem
    {
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public StockAction Action { get; set; }
    }
}
