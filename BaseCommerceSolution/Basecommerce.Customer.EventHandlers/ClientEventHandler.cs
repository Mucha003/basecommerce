﻿using Basecommerce.Customer.DAL;
using Basecommerce.Customer.EventHandlers.Commands;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Basecommerce.Customer.EventHandlers
{
    public class ClientEventHandler : INotificationHandler<ClientCreateCommand>
    {
        private readonly CustomerContext _context;

        public ClientEventHandler(CustomerContext context)
        {
            _context = context;
        }


        public async Task Handle(ClientCreateCommand notification, CancellationToken cancellationToken)
        {
            await _context.AddAsync(new Client
            {
                Name = notification.Name
            });

            await _context.SaveChangesAsync();
        }
    }
}
