﻿using MediatR;

namespace Basecommerce.Customer.EventHandlers.Commands
{
    public class ClientCreateCommand : INotification
    {
        public string Name { get; set; }
    }
}
