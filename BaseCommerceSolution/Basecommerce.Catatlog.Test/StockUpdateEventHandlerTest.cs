using Basecommerce.Catatlog.EventHandlers;
using Basecommerce.Catatlog.EventHandlers.Commands;
using Basecommerce.Catatlog.EventHandlers.ManageError;
using Basecommerce.Catatlog.Test.Config;
using Basecommerce.Models.Enums;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Basecommerce.Catatlog.Test
{
    [TestClass]
    public class StockUpdateEventHandlerTest
    {
        // Chaque methode est un cas distinc de cette classe
        // Creation des DB en Memoire: nuget: Microsoft.EntityFrameworkCore.InMemory
        // nuget: Moq: create fake objet for test.
        // click droit debug test

        private ILogger<StockUpdateEventHandler> GetLogger
        {
            get => new Mock<ILogger<StockUpdateEventHandler>>().Object;
        }


        [TestMethod]
        public void TryToSubstractStockWhenProductHasStock()
        {
            var context = CatalogContextInMemory.Get();
            var stockId = 1;
            var ProductId = 1;

            context.Stocks.Add(new DAL.Stock 
            {
                Id = stockId,
                ProductId = ProductId,
                Quantity = 1
            });

            context.SaveChanges();


            var handler = new StockUpdateEventHandler(context, GetLogger);
            handler.Handle(new StockUpdateCommand
            {
                Items = new List<StockUpdateItem>()
                {
                    new StockUpdateItem()
                    {
                        ProductId = ProductId,
                        Quantity = 1,
                        Action = StockAction.Substract
                    }
                }
            }, new CancellationToken()).Wait();
        }



        // ExpectedException : Le type de retour Que nous avons besoin pour valider ce test.
        [TestMethod]
        [ExpectedException(typeof(StockUpdateCommandError))]
        public void TryToSubstractStockWhenProductHasNotStock()
        {
            var context = CatalogContextInMemory.Get();
            var stockId = 2;
            var ProductId = 2;

            context.Stocks.Add(new DAL.Stock
            {
                Id = stockId,
                ProductId = ProductId,
                Quantity = 1
            });

            context.SaveChanges();


            try
            {
                var handler = new StockUpdateEventHandler(context, GetLogger);
                handler.Handle(new StockUpdateCommand
                {
                    Items = new List<StockUpdateItem>()
                {
                    new StockUpdateItem()
                    {
                        ProductId = ProductId,
                        Quantity = 2,
                        Action = StockAction.Substract
                    }
                }
                }, new CancellationToken()).Wait();
            }
            catch (AggregateException ex) 
            {
                // AggregateException : car une metho asyn envoi ce genre d'exception
                var exception = ex.GetBaseException();

                if (exception is StockUpdateCommandError)
                {
                    throw new StockUpdateCommandError(exception?.InnerException?.Message);
                }
            }
        }



        [TestMethod]
        public void TryToAddStockWhenProductExist()
        {
            var context = CatalogContextInMemory.Get();
            var stockId = 3;
            var ProductId = 3;

            context.Stocks.Add(new DAL.Stock
            {
                Id = stockId,
                ProductId = ProductId,
                Quantity = 1
            });

            context.SaveChanges();


            var handler = new StockUpdateEventHandler(context, GetLogger);
            handler.Handle(new StockUpdateCommand
            {
                Items = new List<StockUpdateItem>()
                {
                    new StockUpdateItem()
                    {
                        ProductId = ProductId,
                        Quantity = 2,
                        Action = StockAction.Add
                    }
                }
            }, new CancellationToken()).Wait();

            var stockInDb = context.Stocks.Single(x => x.ProductId == ProductId).Quantity;
            Assert.AreEqual(stockInDb, 3);
        }



        [TestMethod]
        public void TryToAddStockWhenProductNOTExist()
        {
            var context = CatalogContextInMemory.Get();
            var ProductId = 4;

            var handler = new StockUpdateEventHandler(context, GetLogger);
            handler.Handle(new StockUpdateCommand
            {
                Items = new List<StockUpdateItem>()
                {
                    new StockUpdateItem()
                    {
                        ProductId = ProductId,
                        Quantity = 2,
                        Action = StockAction.Add
                    }
                }
            }, new CancellationToken()).Wait();

            var stockInDb = context.Stocks.Single(x => x.ProductId == ProductId).Quantity;
            Assert.AreEqual(stockInDb, 2);
        }

    }
}
