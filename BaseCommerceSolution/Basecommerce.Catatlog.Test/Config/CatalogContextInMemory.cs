﻿using Basecommerce.Catatlog.DAL;
using Microsoft.EntityFrameworkCore;

namespace Basecommerce.Catatlog.Test.Config
{
    public static class CatalogContextInMemory
    {
        // notre db context pour travailler sur une db en memoire
        public static CatalogContext Get()
        {
            // cette var est le resultat du param du ctor lors de la cretion du CatalogContext dans la DAL
            DbContextOptions<CatalogContext> options = new DbContextOptionsBuilder<CatalogContext>()
                            .UseInMemoryDatabase(databaseName: $"CatalogTest.db")
                            .Options;

            return new CatalogContext(options);
        }

    }
}
