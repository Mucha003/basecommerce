﻿using System;
using System.Collections.Generic;

namespace Basecommerce.Catatlog.DAL.Seed
{
    public class SeedData
    {
        public static List<Product> CreateProducts() 
        {
            List<Product> products = new List<Product>();
            var random = new Random();

            for (int i = 0; i < 10; i++)
            {
                products.Add(new Product
                {
                    Id = i,
                    Name = $"Product {i}",
                    Description = $"Description {i} - du product {i}",
                    Price = random.Next(100, 1000)
                });
            }

            return products;
        }

        public static List<Stock> CreateStocks()
        {
            List<Stock> Stocks = new List<Stock>();
            var random = new Random();

            for (int i = 0; i < 10; i++)
            {
                Stocks.Add(new Stock()
                {
                    Id = i,
                    ProductId = i,
                    Quantity = random.Next(1, 100)
                });
            }

            return Stocks;
        }

    }
}
