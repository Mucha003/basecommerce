﻿using System.ComponentModel.DataAnnotations;

namespace Basecommerce.Catatlog.DAL
{
    public class Stock
    {
        [Key]
        public int Id { get; set; }
        public int Quantity { get; set; }

        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
    }
}