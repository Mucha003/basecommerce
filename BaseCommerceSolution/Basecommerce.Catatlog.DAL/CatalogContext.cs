﻿using Microsoft.EntityFrameworkCore;

namespace Basecommerce.Catatlog.DAL
{
    public class CatalogContext : DbContext
    {
        public CatalogContext(DbContextOptions<CatalogContext> opt)
            : base(opt)
        { }


        public DbSet<Product> Products { get; set; }
        public DbSet<Stock> Stocks { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Schema
            modelBuilder.HasDefaultSchema("Catalog");

            modelBuilder.Entity<Product>().HasData(new Product
            {
                Id = 1,
                Name = $"Product 1",
                Description = $"Description 1 - du product 1",
                Price = 100
            });
            modelBuilder.Entity<Stock>().HasData(new Stock()
            {
                Id = 1,
                ProductId = 1,
                Quantity = 10
            });

            //modelBuilder.Entity<Product>().HasData(SeedData.CreateProducts());
            //modelBuilder.Entity<Stock>().HasData(SeedData.CreateStocks());
        }

    }
}
