﻿using Basecommerce.Common;
using Basecommerce.Customer.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Basecommerce.Customer.BL.Repository
{
    public interface ICustomerRepos
    {
        Task<DataCollection<ClientDto>> GetAllAsync(int page, int take, IEnumerable<int> clients = null);
        Task<ClientDto> GetAsync(int id);
    }
}
