﻿using Basecommerce.Common;
using Basecommerce.Customer.BL.Repository;
using Basecommerce.Customer.DAL;
using Basecommerce.Customer.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Basecommerce.Customer.BL
{
    public class CustomerBL : ICustomerRepos
    {
        private readonly CustomerContext _context;

        public CustomerBL(CustomerContext context)
        {
            _context = context;
        }


        public async Task<DataCollection<ClientDto>> GetAllAsync(int page, int take, IEnumerable<int> clients = null)
        {
            var collection = await _context.Clients
                .Where(x => clients == null || clients.Contains(x.Id))
                .OrderBy(x => x.Name)
                .GetPagedAsync(page, take);

            var resultat = collection.MapTo<DataCollection<ClientDto>>();
            
            return resultat;
        }


        public async Task<ClientDto> GetAsync(int id)
        {
            return (await _context.Clients.SingleAsync(x => x.Id == id)).MapTo<ClientDto>();
        }

    }
}
