﻿using Basecommerce.Catatlog.BL.Repository;
using Basecommerce.Catatlog.EventHandlers.Commands;
using Basecommerce.Common;
using Basecommerce.Models.Catatlog;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Basecommerce.Catatlog.Api.Controllers
{
    [ApiController]
    [Route("Products")]
    public class ProductsController : ControllerBase
    {
        private readonly ILogger<ProductsController> _logger;
        private readonly IProductRepos _repos;
        private readonly IMediator _mediator; // relie nos commands a nos events.

        public ProductsController(
            ILogger<ProductsController> logger,
            IProductRepos repos,
            IMediator mediator)
        {
            _logger = logger;
            _repos = repos;
            _mediator = mediator;
        }


        [HttpGet]
        public async Task<DataCollection<ProductDto>> GetAllProducts(int page = 1, int take = 10, string Ids = null)
        {
            IEnumerable<int> productIds = null;

            if (!string.IsNullOrEmpty(Ids))
            {
                productIds = Ids.Split(',').Select(x => Convert.ToInt32(x));
            }

            return await _repos.GetAllProductsAsync(page, take, productIds);
        }


        [HttpGet("{Id}")]
        public async Task<ProductDto> GetProduct(int Id)
        {
            return await _repos.GetProductsByIdAsync(Id);
        }


        [HttpPost]
        public async Task<IActionResult> Create(ProductCreateCommand command)
        {
            await _mediator.Publish(command);
            return Ok();
        }
    }
}