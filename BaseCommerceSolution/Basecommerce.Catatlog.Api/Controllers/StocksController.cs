﻿using Basecommerce.Catatlog.EventHandlers.Commands;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Basecommerce.Catatlog.Api.Controllers
{
    [ApiController]
    [Route("Stocks")]
    public class StocksController : ControllerBase
    {
        private readonly ILogger<StocksController> _logger;
        private readonly IMediator _mediator; // relie nos commands a nos events.

        public StocksController(ILogger<StocksController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }


        //HttpPut mets a jour ou le cree ou as ou il n'existe pas
        [HttpPut]
        public async Task<IActionResult> UpdateStock(StockUpdateCommand command)
        {
            // postman body: {"items": [{"productId": 1, "quantity": 3, "action": 1}]}
            await _mediator.Publish(command);
            return NoContent();
        }
    }
}