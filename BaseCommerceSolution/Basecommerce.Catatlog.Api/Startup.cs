using Basecommerce.Catatlog.BL;
using Basecommerce.Catatlog.BL.Repository;
using Basecommerce.Catatlog.DAL;
using Basecommerce.Common.Logging.Extensions;
using HealthChecks.UI.Client;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Reflection;

namespace Basecommerce.Catatlog.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }


        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<CatalogContext>(opt =>
            {
                opt.UseSqlServer(Configuration.GetConnectionString("CatalogDB"),
                    // pour garder les migrations dans le schema Catalog
                    x => x.MigrationsHistoryTable("_EfMigrationCatalog", "Catalog"));
            });

            // 1. fisrt step, suit dans app.
            // Health Checks: check que tout soi Okk
            // nous renvoi ce status code (safe)
            // Si notre api es down, ceci ne s'execute pas (renvoi error 500).
            // check, ex: si ce microservice communique avec une autre on peut lui ajouter un PING, check DB, etc..
            services.AddHealthChecks()
                .AddCheck("self", () => HealthCheckResult.Healthy())
                .AddDbContextCheck<CatalogContext>();// check la connection a la db

            services.AddHealthChecksUI();

            // Assembly ira lire tous nos commands dans ce projet, comme une connectionString
            services.AddMediatR(Assembly.Load("Basecommerce.Catatlog.EventHandlers"));

            services.AddTransient<IProductRepos, ProductBL>();

            services.AddControllers();
        }


        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                loggerFactory.AddSyslog(
                    Configuration.GetValue<string>("Papertrail:host"),
                    Configuration.GetValue<int>("Papertrail:port"));
            }


            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                // 2. healtCheck : /hc = route de ce endpoint
                // ex: https://localhost:44339/hc
                endpoints.MapHealthChecks("/hc", new HealthCheckOptions()
                {
                    Predicate = _ => true,
                    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                });
                endpoints.MapHealthChecksUI();
                endpoints.MapControllers();
            });
        }
    }
}
