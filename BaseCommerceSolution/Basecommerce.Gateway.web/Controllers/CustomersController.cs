﻿using Basecommerce.Common;
using Basecommerce.Gateway.Models.Customers;
using Basecommerce.Gateway.Proxies.Customer;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Basecommerce.Gateway.web.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerProxy _customerProxy;

        public CustomersController(ICustomerProxy customerProxy)
        {
            _customerProxy = customerProxy;
        }



        [HttpGet]
        public async Task<DataCollection<ClientDto>> GetAll(int page, int take)
        {
            return await _customerProxy.GetAllAsync(page, take);
        }



        [HttpGet("{id}")]
        public async Task<ClientDto> Get(int id)
        {
            return await _customerProxy.GetAsync(id);
        }
    }
}