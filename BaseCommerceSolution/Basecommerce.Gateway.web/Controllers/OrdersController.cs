﻿using Basecommerce.Common;
using Basecommerce.Gateway.Models;
using Basecommerce.Gateway.Models.Order;
using Basecommerce.Gateway.Proxies.Catalog;
using Basecommerce.Gateway.Proxies.Customer;
using Basecommerce.Gateway.Proxies.Order;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace Basecommerce.Gateway.web.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderProxy _orderProxy;
        private readonly ICustomerProxy _customerProxy;
        private readonly ICatalogProxy _catalogProxy;

        public OrdersController(IOrderProxy orderProxy, ICustomerProxy customerProxy, ICatalogProxy catalogProxy)
        {
            _orderProxy = orderProxy;
            _customerProxy = customerProxy;
            _catalogProxy = catalogProxy;
        }



        /// <summary>
        /// Este método no necesita traer la información de los productos porque lo usaremos para solo mostrar
        /// las cabeceras en el listado. RECUERDA: que este API Gateway alimenta a nuestro Web.Client - El backend de nuestro frontend
        /// </summary>
        /// <param name="page"></param>
        /// <param name="take"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<DataCollection<OrderResponse>> GetAll(int page, int take)
        {
            //1. j'apporter mes orders.
            var result = await _orderProxy.GetAllAsync(page, take);

            if (result.HasItems)
            {
                // Retrieve client ids
                var clientIds = result.Items
                    .Select(x => x.ClientId)
                    .GroupBy(g => g)
                    .Select(x => x.Key).ToList();

                var clients = await _customerProxy.GetAllAsync(1, clientIds.Count(), clientIds);

                foreach (var order in result.Items)
                {
                    var rest = clients.Items.FirstOrDefault(x => x.Id == order.ClientId);
                    order.Client = clients.Items.FirstOrDefault(x => x.Id == order.ClientId);
                }
            }

            return result;
        }



        [HttpGet("{id}")]
        public async Task<OrderResponse> Get(int id)
        {
            //1. j'apporter mes orders.
            var result = await _orderProxy.GetAsync(id);

            // 2. j'apporter mes client
            result.Client = await _customerProxy.GetAsync(result.ClientId);

            // products ids de l'order
            var productIds = result.Items
                                   .Select(x => x.ProductId)
                                   .GroupBy(g => g)
                                   .Select(x => x.Key).ToList();

            // 3. j'apporter mes products 
            var products = await _catalogProxy.GetAllAsync(1, productIds.Count(), productIds);

            foreach (var item in result.Items)
            {
                item.Product = products.Items.Single(x => x.Id == item.ProductId);
            }

            return result;
        }



        [HttpPost]
        public async Task<IActionResult> Create(OrderCreateDto command)
        {
            // https://localhost:44325/orders
            await _orderProxy.CreateAsync(command);
            return Ok();
        }

    }
}