﻿using Microsoft.AspNetCore.Mvc;

namespace Basecommerce.Gateway.web.Controllers
{
    [Route("/")]
    [ApiController]
    public class DefaultController : ControllerBase
    {
        [HttpGet]
        public ActionResult<string> Get()
        {
            return "Getaway Api Running .....";
        }
    }
}