﻿using Basecommerce.Common;
using Basecommerce.Order.BL.Repository;
using Basecommerce.Order.EventHandlers.Commands;
using Basecommerce.Order.Models;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Basecommerce.Order.Api.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderRepos _orderRepos;
        private readonly ILogger<OrdersController> _logger;
        private readonly IMediator _mediator;


        public OrdersController(
            ILogger<OrdersController> logger,
            IMediator mediator,
            IOrderRepos OrderRepos)
        {
            _logger = logger;
            _mediator = mediator;
            _orderRepos = OrderRepos;
        }


        [HttpGet]
        public async Task<DataCollection<OrderDto>> GetAll(int page = 1, int take = 10)
        {
            return await _orderRepos.GetAllAsync(page, take);
        }



        [HttpGet("{id}")]
        public async Task<OrderDto> Get(int id)
        {
            return await _orderRepos.GetAsync(id);
        }



        [HttpPost]
        public async Task<IActionResult> Create(OrderCreateCommand notification)
        {
            await _mediator.Publish(notification);
            return Ok();
        }
    }
}