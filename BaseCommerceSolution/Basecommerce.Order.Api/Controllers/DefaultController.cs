﻿using Microsoft.AspNetCore.Mvc;

namespace Basecommerce.Order.Api.Controllers
{
    [ApiController]
    [Route("/")]
    public class DefaultController : ControllerBase
    {
        [HttpGet]
        public string Index()
        {
            return "Order Api Running ..........";
        }
    }
}