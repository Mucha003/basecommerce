using Basecommerce.Common.Logging.Extensions;
using Basecommerce.Order.BL;
using Basecommerce.Order.BL.Repository;
using Basecommerce.Order.DAL;
using Basecommerce.Order.Models;
using Basecommerce.Order.Proxies.Catalog;
using HealthChecks.UI.Client;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System.Reflection;
using System.Text;

namespace Basecommerce.Order.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }



        public void ConfigureServices(IServiceCollection services)
        {
            // HttpContextAccessor
            services.AddHttpContextAccessor();

            // DbContext
            services.AddDbContext<OrderContext>(
                options => options.UseSqlServer(
                    Configuration.GetConnectionString("ContextDB"),
                    x => x.MigrationsHistoryTable("__EFMigrationsHistory", "Order")
                )
            );

            // Health check
            services.AddHealthChecks()
                        .AddCheck("self", () => HealthCheckResult.Healthy())
                        .AddDbContextCheck<OrderContext>(typeof(OrderContext).Name);

            services.AddHealthChecksUI();

            // Api URLS
            services.Configure<ApiUrls>(opts => Configuration.GetSection("ApiProxies").Bind(opts));
            services.Configure<AzureServiceBus>(opts => Configuration.GetSection("AzureServiceBus").Bind(opts));


            // Proxies
            //services.AddHttpClient<ICatalogProxy, CatalogHttpProxy>();
            services.AddTransient<ICatalogProxy, CatalogQueueProxy>();

            // Event handlers
            services.AddMediatR(Assembly.Load("Basecommerce.Order.EventHandlers"));

            // Repos
            services.AddTransient<IOrderRepos, OrderBL>();


            services.AddControllers();

            // Add Authentication
            byte[] secretKey = Encoding.ASCII.GetBytes(Configuration.GetValue<string>("SecretKey"));

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(secretKey),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
        }



        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                loggerFactory.AddSyslog(
                    Configuration.GetValue<string>("Papertrail:host"),
                    Configuration.GetValue<int>("Papertrail:port"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                // 2. healtCheck : /hc = route de ce endpoint
                // ex: https://localhost:44339/hc
                endpoints.MapHealthChecks("/hc", new HealthCheckOptions()
                {
                    Predicate = _ => true,
                    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                });

                endpoints.MapHealthChecksUI();
                endpoints.MapControllers();
            });
        }
    }
}
