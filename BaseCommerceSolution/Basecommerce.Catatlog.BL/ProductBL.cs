﻿using Basecommerce.Catatlog.BL.Repository;
using Basecommerce.Catatlog.DAL;
using Basecommerce.Common;
using Basecommerce.Models.Catatlog;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Basecommerce.Catatlog.BL
{
    public class ProductBL : IProductRepos
    {
        private readonly CatalogContext _context;

        public ProductBL(CatalogContext context)
        {
            _context = context;
        }


        /// <summary>
        /// return all Products du Catalog
        /// </summary>
        /// <param name="page">Init Page</param>
        /// <param name="take">Quantites de registre a apporter</param>
        /// <param name="products">trier par des produits specific(Id)</param>
        /// <returns></returns>
        public async Task<DataCollection<ProductDto>> GetAllProductsAsync(int page, int take, IEnumerable<int> products = null)
        {
            var productsDB = await _context.Products
                                            .Where(x => products == null || products.Contains(x.Id))
                                            .OrderByDescending(x => x.Id)
                                            .GetPagedAsync(page, take); // ceci a ete cree par nous pour la pagination.

            // MapTo: est notre classe de transformation de Classe.
            return productsDB.MapTo<DataCollection<ProductDto>>();
        }


        public async Task<ProductDto> GetProductsByIdAsync(int Id)
        {
            return (await _context.Products.SingleAsync(x => x.Id == Id)).MapTo<ProductDto>();
        }
    }
}
