﻿using Basecommerce.Common;
using Basecommerce.Models.Catatlog;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Basecommerce.Catatlog.BL.Repository
{
    public interface IProductRepos
    {
        Task<DataCollection<ProductDto>> GetAllProductsAsync(int page, int take, IEnumerable<int> products = null);
        Task<ProductDto> GetProductsByIdAsync(int Id);
    }
}
