							INstalls
						--------------
Nugets Api	
	- MediatR
	- MediatR.Extensions.Microsoft.DependencyInjection
	- Microsoft.Extensions.Diagnostics.HealthChecks.EntityFrameworkCore
	- AspNetCore.HealthChecks.UI
	- AspNetCore.HealthChecks.UI.Client
	- Microsoft.EntityFrameworkCore

Nugets DAL
	- Microsoft.EntityFrameworkCore.SqlServer

Nugets EventHandlers
	- MediatR

Nugets Test
	- Microsoft.EntityFrameworkCore.InMemory
	- Moq

Nugets Common.Logging
	- Microsoft.Extensions.Logging
	- Microsoft.Extensions.Logging.Abstractions

Nugets Proxies
	- Microsoft.Extensions.Options
	- Microsoft.Azure.ServiceBus