﻿using Basecommerce.Order.Models;
using Basecommerce.Order.Models.Extentions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Basecommerce.Order.Proxies.Catalog
{
    public class CatalogHttpProxy : ICatalogProxy
    {
        private readonly ApiUrls _url;
        private readonly HttpClient _httpClient;

        public CatalogHttpProxy(IOptions<ApiUrls> Url, HttpClient HttpClient, IHttpContextAccessor HttpContextAccessor)
        {
            HttpClient.AddBearerToken(HttpContextAccessor);
            _url = Url.Value;
            _httpClient = HttpClient;
        }


        //// Call Synchrone by Api.
        public async Task UpdateStockAsync(StockUpdate model)
        {
            //Request Catlog StocksController
            StringContent content = new StringContent(
                JsonSerializer.Serialize(model),
                Encoding.UTF8,
                "application/json"
            );

            HttpResponseMessage request = await _httpClient.PutAsync(_url.CatalogUrl + "Stocks", content);
            // dans le cas que le retun ne soit pas 200 cette methode va envoyer une erreur.
            request.EnsureSuccessStatusCode();
        }

    }
}
