﻿using Basecommerce.Order.Models;
using System.Threading.Tasks;

namespace Basecommerce.Order.Proxies.Catalog
{
    public interface ICatalogProxy
    {
        Task UpdateStockAsync(StockUpdate model);
    }
}
