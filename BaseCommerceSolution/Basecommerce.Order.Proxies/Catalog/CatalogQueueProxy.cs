﻿using Basecommerce.Order.Models;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Options;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Basecommerce.Order.Proxies.Catalog
{
    public class CatalogQueueProxy : ICatalogProxy
    {
        private readonly string _connectionString;

        public CatalogQueueProxy(IOptions<AzureServiceBus> azure)
        {
            _connectionString = azure.Value.ConnectionString;
        }


        //// Call Asynchrone by Queue.
        public async Task UpdateStockAsync(StockUpdate model)
        {
            // Connection a la Queue.
            QueueClient queueClient = new QueueClient(_connectionString, "order-stock-update");


            //// Serialize message
            string body = JsonSerializer.Serialize(model);
            Message message = new Message(Encoding.UTF8.GetBytes(body));


            //// Serialize message
            await queueClient.SendAsync(message);


            //// Close
            await queueClient.CloseAsync();

        }

    }
}
