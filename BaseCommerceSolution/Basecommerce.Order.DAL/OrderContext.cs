﻿using Microsoft.EntityFrameworkCore;

namespace Basecommerce.Order.DAL
{
    public class OrderContext : DbContext
    {
        public OrderContext(DbContextOptions<OrderContext> opt)
            : base(opt)
        {
        }

        public DbSet<Order> Orders { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Schema
            modelBuilder.HasDefaultSchema("Order");
        }

    }
}
