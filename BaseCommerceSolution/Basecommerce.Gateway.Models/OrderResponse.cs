﻿using Basecommerce.Gateway.Models.Customers;
using Basecommerce.Gateway.Models.Enums;
using System;
using System.Collections.Generic;

namespace Basecommerce.Gateway.Models
{
    public class OrderResponse
    {
        public int OrderId { get; set; }
        public string OrderNumber { get; set; }
        public OrderStatus Status { get; set; }
        public OrderPayment PaymentType { get; set; }
        public int ClientId { get; set; }
        public DateTime CreatedAt { get; set; }
        public decimal Total { get; set; }

        public IEnumerable<OrderDetailResponse> Items { get; set; } = new List<OrderDetailResponse>();
        public ClientDto Client { get; set; }
    }
}
