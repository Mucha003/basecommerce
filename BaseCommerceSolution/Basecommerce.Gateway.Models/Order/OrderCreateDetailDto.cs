﻿namespace Basecommerce.Gateway.Models.Order
{
    public class OrderCreateDetailDto
    {
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
    }
}
