﻿using Basecommerce.Gateway.Models.Enums;
using System.Collections.Generic;

namespace Basecommerce.Gateway.Models.Order
{
    public class OrderCreateDto
    {
        public OrderPayment PaymentType { get; set; }
        public int ClientId { get; set; }
        public IEnumerable<OrderCreateDetailDto> Items { get; set; } = new List<OrderCreateDetailDto>();
    }
}
