﻿namespace Basecommerce.Gateway.Models.Catatlog
{
    public class StockDto
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public int ProductId { get; set; }
    }
}