﻿namespace Basecommerce.Gateway.Models.Enums
{
    public enum OrderStatus
    {
        Cancel,
        Pending,
        Approved
    }
}
