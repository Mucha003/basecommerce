﻿namespace Basecommerce.Gateway.Models.Enums
{
    public enum OrderPayment
    {
        CreditCard,
        PayPal,
        BankTransfer
    }
}
