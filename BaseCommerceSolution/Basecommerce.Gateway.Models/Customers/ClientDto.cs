﻿namespace Basecommerce.Gateway.Models.Customers
{
    public class ClientDto
    {
        //public int ClientId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
