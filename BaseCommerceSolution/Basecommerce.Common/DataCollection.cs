﻿using System.Collections.Generic;
using System.Linq;

namespace Basecommerce.Common
{
    // Pour la Pagination
    public class DataCollection<T>
    {
        public IEnumerable<T> Items { get; set; } // Notre Liste d'element

        public int Total { get; set; } // Total elements

        public int Page { get; set; } // La page ou il se trouve

        public int Pages { get; set; } // quantites des pages qui a cette Pagination

        public bool HasItems => Items != null && Items.Any();
    }
}
