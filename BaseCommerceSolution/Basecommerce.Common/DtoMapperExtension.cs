﻿using System.Text.Json;

namespace Basecommerce.Common
{
    public static class DtoMapperExtension
    {
        public static T MapTo<T>(this object value)
        {
            // (Jute pour ici) Serealize une class a une autre tant que les props soient les mms.
            // Utiliser AutoMapper.
            return JsonSerializer.Deserialize<T>(JsonSerializer.Serialize(value));
        }
    }
}
