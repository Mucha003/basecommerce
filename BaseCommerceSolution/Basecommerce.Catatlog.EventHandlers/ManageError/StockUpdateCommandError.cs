﻿using System;

namespace Basecommerce.Catatlog.EventHandlers.ManageError
{
    public class StockUpdateCommandError : Exception
    {
        public StockUpdateCommandError(string message)
            : base(message)
        {
        }

    }
}
