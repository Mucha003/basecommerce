﻿using Basecommerce.Catatlog.DAL;
using Basecommerce.Catatlog.EventHandlers.Commands;
using Basecommerce.Catatlog.EventHandlers.ManageError;
using Basecommerce.Models.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Basecommerce.Catatlog.EventHandlers
{
    public class StockUpdateEventHandler : INotificationHandler<StockUpdateCommand>
    {
        private readonly CatalogContext _context;
        private readonly ILogger<StockUpdateEventHandler> _logger;

        public StockUpdateEventHandler(CatalogContext context, ILogger<StockUpdateEventHandler> logger)
        {
            _context = context;
            _logger = logger;
        }


        public async Task Handle(StockUpdateCommand command, CancellationToken cancellationToken)
        {
            _logger.LogInformation("--- StockUpdateCommand started");

            IEnumerable<int> products = command.Items
                                                .Select(x => x.ProductId);

            List<Stock> stocks = await _context.Stocks
                                                .Where(x => products.Contains(x.ProductId))
                                                .ToListAsync();


            _logger.LogInformation("--- Retrieve products from database");


            foreach (StockUpdateItem item in command.Items)
            {
                Stock entry = stocks.SingleOrDefault(x => x.ProductId == item.ProductId);

                if (item.Action == StockAction.Substract)
                {
                    if (entry == null || item.Quantity > entry.Quantity)
                    {
                        _logger.LogError($"--- Product {entry.ProductId} -doens't have enough stock");
                        throw new StockUpdateCommandError($"Product {entry.ProductId} - doens't have enough stock");
                    }

                    entry.Quantity -= item.Quantity;

                    _logger.LogInformation($"--- Product {entry.ProductId} - its stock was subtracted and its new stock is {entry.Quantity}");
                }
                else
                {
                    if (entry == null)
                    {
                        entry = new Stock
                        {
                            ProductId = item.ProductId
                        };

                        _logger.LogInformation($"--- New stock record was created for {entry.ProductId} because didn't exists before");

                        await _context.AddAsync(entry);
                    }

                    _logger.LogInformation($"--- Add stock to product {entry.ProductId}");
                    entry.Quantity += item.Quantity;
                }
            }

            await _context.SaveChangesAsync();
            _logger.LogInformation("--- StockUpdateCommand ended");
        }
    }
}
