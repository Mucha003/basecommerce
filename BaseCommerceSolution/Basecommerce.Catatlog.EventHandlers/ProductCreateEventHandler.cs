﻿using Basecommerce.Catatlog.DAL;
using Basecommerce.Catatlog.EventHandlers.Commands;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Basecommerce.Catatlog.EventHandlers
{
    public class ProductCreateEventHandler : INotificationHandler<ProductCreateCommand>
    {
        private readonly CatalogContext _context;

        public ProductCreateEventHandler(CatalogContext context)
        {
            _context = context;
        }


        // Execute le command
        public async Task Handle(ProductCreateCommand model, CancellationToken cancellationToken)
        {
            await _context.AddAsync(new Product
            {
                Name = model.Name,
                Description = model.Description,
                Price = model.Price,
            });

            await _context.SaveChangesAsync();
        }

    }
}
