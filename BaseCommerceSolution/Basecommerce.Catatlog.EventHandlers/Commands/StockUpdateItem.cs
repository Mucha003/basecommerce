﻿using Basecommerce.Models.Enums;

namespace Basecommerce.Catatlog.EventHandlers.Commands
{
    public class StockUpdateItem
    {
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public StockAction Action { get; set; }
    }
}
