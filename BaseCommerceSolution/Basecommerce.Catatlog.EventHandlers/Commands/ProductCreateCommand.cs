﻿using MediatR;

namespace Basecommerce.Catatlog.EventHandlers.Commands
{
    // Enregistrer le product
    public class ProductCreateCommand : INotification
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
    }
}
