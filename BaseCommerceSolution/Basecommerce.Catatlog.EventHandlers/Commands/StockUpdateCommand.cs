﻿using MediatR;
using System.Collections.Generic;

namespace Basecommerce.Catatlog.EventHandlers.Commands
{
    public class StockUpdateCommand : INotification
    {
        public IEnumerable<StockUpdateItem> Items { get; set; } = new List<StockUpdateItem>();
    }
}
