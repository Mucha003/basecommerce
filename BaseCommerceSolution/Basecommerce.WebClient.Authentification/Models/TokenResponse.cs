﻿namespace Basecommerce.WebClient.Authentification.Models
{
    public class TokenResponse
    {
        public string AccessToken { get; set; }
        public bool Succeeded { get; set; }
    }
}
