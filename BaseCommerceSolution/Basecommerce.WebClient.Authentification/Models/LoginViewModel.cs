﻿using System.ComponentModel.DataAnnotations;

namespace Basecommerce.WebClient.Authentification.Models
{
    public class LoginViewModel
    {
        [Required]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool HasInvalidAccess { get; set; }
    }
}
