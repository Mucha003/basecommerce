﻿using Basecommerce.WebClient.Authentification.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Basecommerce.WebClient.Authentification.Controllers
{
    [Route("/")]
    public class AuthController : Controller
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly ILogger<AuthController> _logger;

        public AuthController(IHttpClientFactory clientFactory, ILogger<AuthController> logger)
        {
            _clientFactory = clientFactory;
            _logger = logger;
        }



        [HttpGet]
        public IActionResult Login()
        {
            LoginViewModel model = new LoginViewModel
            {
                HasInvalidAccess = false
            };

            return View(model);
        }



        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            string prefixe = "api/auth/Login";

            StringContent cotent = new StringContent(
                JsonSerializer.Serialize(model),
                Encoding.UTF8,
                "application/json");

            HttpResponseMessage request = await _clientFactory.CreateClient("AuthApi")
                                                              .PostAsync(prefixe, cotent);

            if (!request.IsSuccessStatusCode)
            {
                model.HasInvalidAccess = true;
                return View(model);
            }

            TokenResponse response = JsonSerializer.Deserialize<TokenResponse>(
                    await request.Content.ReadAsStringAsync(),
                    new JsonSerializerOptions
                    {
                        // permet d'eviter les erreur de Upper/Lower des props Json.
                        PropertyNameCaseInsensitive = true
                    }
                );

            return Redirect($"https://localhost:44373/Account/connect?access_token={response.AccessToken}");

        }
    }
}