﻿namespace Basecommerce.Common.Logging.Enums
{
    public enum SyslogLogLevel
    {
        Emergency,
        Alert,
        Critical,
        Error,
        Warn,
        Notice,
        Info,
        Debug
    }
}
