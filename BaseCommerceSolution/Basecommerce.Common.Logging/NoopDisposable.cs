﻿using System;

namespace Basecommerce.Common.Logging
{
    public class NoopDisposable : IDisposable
    {
        public static NoopDisposable Instance = new NoopDisposable();
        public void Dispose()
        {
        }
    }
}
