﻿namespace Basecommerce.Identity.Api.Models
{
    public class IdentityAccessResponse
    {
        public bool Succeeded { get; set; }
        public string AccessToken { get; set; }
    }
}
