﻿using Basecommerce.Identity.Api.DAL;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Basecommerce.Identity.Api.TableConfig
{
    public class UserTableConfig
    {

        public UserTableConfig(EntityTypeBuilder<UserModel> entityBuilder)
        {
            entityBuilder.HasKey(x => x.Id);


            entityBuilder.Property(x => x.FirstName)
                         .IsRequired()
                         .HasMaxLength(100);


            entityBuilder.Property(x => x.LastName)
                         .IsRequired()
                         .HasMaxLength(100);


            entityBuilder.HasMany(e => e.UserRoles)
                         .WithOne(e => e.User)
                         .HasForeignKey(e => e.UserId)
                         .IsRequired();
        }
    }
}
