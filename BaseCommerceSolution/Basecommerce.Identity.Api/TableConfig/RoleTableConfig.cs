﻿using Basecommerce.Identity.Api.DAL;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace Basecommerce.Identity.Api.TableConfig
{
    public class RoleTableConfig
    {
        public RoleTableConfig(EntityTypeBuilder<RoleModel> entityBuilder)
        {
            entityBuilder.HasKey(x => x.Id);


            entityBuilder.HasData(
                new RoleModel
                {
                    Id = Guid.NewGuid().ToString().ToLower(),
                    Name = "Admin",
                    NormalizedName = "ADMIN"
                }
            );


            entityBuilder.HasMany(e => e.UserRoles)
                         .WithOne(e => e.Role)
                         .HasForeignKey(e => e.RoleId)
                         .IsRequired();
        }
    }
}
