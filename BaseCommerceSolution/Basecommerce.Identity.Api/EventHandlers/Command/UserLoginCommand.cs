﻿using Basecommerce.Identity.Api.Models;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace Basecommerce.Identity.Api.EventHandlers.Command
{
    public class UserLoginCommand : IRequest<IdentityAccessResponse>
    {
        [Required, EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
