﻿using Basecommerce.Identity.Api.DAL;
using Basecommerce.Identity.Api.EventHandlers.Command;
using MediatR;
using Microsoft.AspNetCore.Identity;
using System.Threading;
using System.Threading.Tasks;

namespace Basecommerce.Identity.Api.EventHandlers
{
    // UserCreateCommand return une response IdentityResult
    public class UserCreateEventHandler : IRequestHandler<UserCreateCommand, IdentityResult>
    {
        private readonly UserManager<UserModel> _userManager;

        public UserCreateEventHandler(UserManager<UserModel> UserManager)
        {
            _userManager = UserManager;
        }


        public async Task<IdentityResult> Handle(UserCreateCommand notification, CancellationToken cancellationToken)
        {
            UserModel entry = new UserModel
            {
                FirstName = notification.FirstName,
                LastName = notification.LastName,
                Email = notification.Email,
                UserName = notification.Email
            };

            // creation with password
            return await _userManager.CreateAsync(entry, notification.Password);
        }

    }
}
