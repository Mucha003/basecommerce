﻿using Basecommerce.Identity.Api.DAL;
using Basecommerce.Identity.Api.EventHandlers.Command;
using Basecommerce.Identity.Api.Models;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Basecommerce.Identity.Api.EventHandlers
{
    public class UserLoginEventHandler : IRequestHandler<UserLoginCommand, IdentityAccessResponse>
    {
        private readonly SignInManager<UserModel> _signInManager;
        private readonly IdentityContext _context;
        private readonly IConfiguration _configuration;

        public UserLoginEventHandler(
           SignInManager<UserModel> signInManager,
           IdentityContext context,
           IConfiguration configuration)
        {
            _signInManager = signInManager;
            _context = context;
            _configuration = configuration;
        }



        public async Task<IdentityAccessResponse> Handle(UserLoginCommand request, CancellationToken cancellationToken)
        {
            IdentityAccessResponse result = new IdentityAccessResponse();

            var user = await _context.Users.SingleAsync(x => x.Email == request.Email);
            var response = await _signInManager.CheckPasswordSignInAsync(user, request.Password, false);

            if (response.Succeeded)
            {
                result.Succeeded = true;
                await GenerateToken(user, result);
            }

            return result;
        }



        private async Task GenerateToken(UserModel user, IdentityAccessResponse identity)
        {
            var secretKey = _configuration.GetValue<string>("SecretKeyToken");
            var key = Encoding.ASCII.GetBytes(secretKey);

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.Name, user.FirstName),
                new Claim(ClaimTypes.Surname, user.LastName)
            };

            var roles = await _context.Roles
                                      .Where(x => x.UserRoles.Any(y => y.UserId == user.Id))
                                      .ToListAsync();

            foreach (var role in roles)
            {
                claims.Add( new Claim(ClaimTypes.Role, role.Name) );
            }

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(key),
                    SecurityAlgorithms.HmacSha256Signature
                )
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var createdToken = tokenHandler.CreateToken(tokenDescriptor);

            identity.AccessToken = tokenHandler.WriteToken(createdToken);
        }


    }
}
