﻿using Basecommerce.Identity.Api.DAL;
using Basecommerce.Identity.Api.EventHandlers.Command;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Basecommerce.Identity.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly ILogger<AuthController> _logger;
        private readonly SignInManager<UserModel> _signInManager;
        private readonly IMediator _mediator;

        public AuthController(
            ILogger<AuthController> logger,
            SignInManager<UserModel> signInManager,
            IMediator mediator)
        {
            _logger = logger;
            _signInManager = signInManager;
            _mediator = mediator;
        }


        [HttpPost("Register")]
        public async Task<IActionResult> Register(UserCreateCommand command)
        {
            // Postman : https://localhost:44332/api/auth/Register 
            // { "firstName" : "Michael", "lastName": "Benavides", "email": "Test@gmail.com", "password": "123456" }
            if (ModelState.IsValid)
            {
                // publish : est de type void
                // Send: pour avoir un return
                var result = await _mediator.Send(command);

                if (!result.Succeeded)
                {
                    return BadRequest(result.Errors);
                }

                return Ok();
            }

            return BadRequest();
        }


        [HttpPost("Login")]
        public async Task<IActionResult> Login(UserLoginCommand command)
        {
            if (ModelState.IsValid)
            {
                var result = await _mediator.Send(command);

                if (!result.Succeeded)
                {
                    return BadRequest("Access denied");
                }

                return Ok(result);
            }

            return BadRequest();
        }

    }
}