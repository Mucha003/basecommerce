﻿using Microsoft.AspNetCore.Mvc;

namespace Basecommerce.Identity.Api.Controllers
{
    [Route("/")]
    [ApiController]
    public class DefaultController : ControllerBase
    {
        [HttpGet]
        public ActionResult<string> Get()
        {
            return "Identity Api Running ............";
        }

    }
}