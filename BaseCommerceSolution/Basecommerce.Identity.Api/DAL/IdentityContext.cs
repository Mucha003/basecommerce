﻿using Basecommerce.Identity.Api.TableConfig;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Basecommerce.Identity.Api.DAL
{
    // < nos class personalizée >
    public class IdentityContext : IdentityDbContext<UserModel, RoleModel, string>
    {
        public IdentityContext(DbContextOptions<IdentityContext> opt)
            : base(opt)
        {
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Schema
            modelBuilder.HasDefaultSchema("Identity");

            // Model Contraints
            ModelConfig(modelBuilder);
        }


        private void ModelConfig(ModelBuilder modelBuilder)
        {
            new UserTableConfig(modelBuilder.Entity<UserModel>());
            new RoleTableConfig(modelBuilder.Entity<RoleModel>());
        }


    }
}
