﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace Basecommerce.Identity.Api.DAL
{
    public class UserModel : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public ICollection<UserRoleModel> UserRoles { get; set; }
    }
}
