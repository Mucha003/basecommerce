﻿using Microsoft.AspNetCore.Identity;

namespace Basecommerce.Identity.Api.DAL
{
    public class UserRoleModel : IdentityUserRole<string>
    {
        public UserModel User { get; set; }
        public RoleModel Role { get; set; }
    }
}
