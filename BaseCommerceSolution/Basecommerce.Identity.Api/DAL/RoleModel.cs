﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace Basecommerce.Identity.Api.DAL
{
    public class RoleModel : IdentityRole
    {
        public ICollection<UserRoleModel> UserRoles { get; set; }
    }
}
