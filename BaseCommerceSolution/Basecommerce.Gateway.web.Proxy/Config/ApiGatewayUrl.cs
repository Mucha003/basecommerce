﻿namespace Basecommerce.Gateway.web.Proxy.Config
{
    public class ApiGatewayUrl
    {
        public readonly string Value;

        public ApiGatewayUrl(string url)
        {
            Value = url;
        }

    }
}