﻿using Basecommerce.Common;
using Basecommerce.Gateway.Models.Catatlog;
using System.Threading.Tasks;

namespace Basecommerce.Gateway.web.Proxy
{
    public interface IProductProxy
    {
        Task<DataCollection<ProductDto>> GetAllAsync(int page, int take);
    }
}
