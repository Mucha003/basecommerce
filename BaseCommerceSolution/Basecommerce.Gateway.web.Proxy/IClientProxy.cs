﻿using Basecommerce.Common;
using Basecommerce.Gateway.Models.Customers;
using System.Threading.Tasks;

namespace Basecommerce.Gateway.web.Proxy
{
    public interface IClientProxy
    {
        Task<DataCollection<ClientDto>> GetAllAsync(int page, int take);
    }
}
