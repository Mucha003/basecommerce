﻿using Basecommerce.Common;
using Basecommerce.Gateway.Models.Order;
using System.Threading.Tasks;

namespace Basecommerce.Gateway.web.Proxy
{
    public interface IOrderProxy
    {
        /// <summary>
        /// Get info des products
        /// </summary>
        /// <param name="page"></param>
        /// <param name="take"></param>
        /// <returns></returns>
        Task<DataCollection<OrderDto>> GetAllAsync(int page, int take);

        /// <summary>
        /// Information complete de la commande avec les diff microservicios.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<OrderDto> GetAsync(int id);

        /// <summary>
        /// Creation orders.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        Task CreateAsync(OrderCreateDto command);
    }
}
