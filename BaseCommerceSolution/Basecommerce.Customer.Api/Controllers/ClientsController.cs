﻿using Basecommerce.Common;
using Basecommerce.Customer.BL.Repository;
using Basecommerce.Customer.EventHandlers.Commands;
using Basecommerce.Customer.Models;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Basecommerce.Customer.Api.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class ClientsController : Controller
    {
        private readonly ICustomerRepos _repos;
        private readonly ILogger<ClientsController> _logger;
        private readonly IMediator _mediator;


        public ClientsController(ICustomerRepos repos, ILogger<ClientsController> logger, IMediator mediator)
        {
            _repos = repos;
            _logger = logger;
            _mediator = mediator;
        }


        [HttpGet]
        public async Task<DataCollection<ClientDto>> GetAll(int page = 1, int take = 10, string ids = null)
        {
            //// Lire les claims qui sont dans le token.
            //// retuen Id de l'user.
            //var id = User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.NameIdentifier)).Value;

            IEnumerable<int> clients = null;

            if (!string.IsNullOrEmpty(ids))
            {
                clients = ids.Split(',').Select(x => Convert.ToInt32(x));
            }

            var result = await _repos.GetAllAsync(page, take, clients);
            return result;
        }



        [HttpGet("{id}")]
        public async Task<ClientDto> Get(int id)
        {
            return await _repos.GetAsync(id);
        }



        [HttpPost]
        public async Task<IActionResult> Create(ClientCreateCommand notification)
        {
            await _mediator.Publish(notification);
            return Ok();
        }

    }
}