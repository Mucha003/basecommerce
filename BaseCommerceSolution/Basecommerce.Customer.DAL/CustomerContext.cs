﻿using Microsoft.EntityFrameworkCore;

namespace Basecommerce.Customer.DAL
{
    public class CustomerContext : DbContext
    {
        public CustomerContext(DbContextOptions<CustomerContext> opt)
            : base(opt)
        {
        }

        public DbSet<Client> Clients { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Schema
            modelBuilder.HasDefaultSchema("Customer");

            modelBuilder.Entity<Client>().HasData(new Client
            {
                Id = 1,
                Name = $"Michael",
            });
        }
    }
}
