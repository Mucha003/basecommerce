﻿using Basecommerce.Order.DAL;
using Basecommerce.Order.EventHandlers.Commands;
using Basecommerce.Order.Models;
using Basecommerce.Order.Models.Enums;
using Basecommerce.Order.Proxies.Catalog;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Basecommerce.Order.EventHandlers
{
    public class OrderCreateEventHandler : INotificationHandler<OrderCreateCommand>
    {
        private readonly OrderContext _context;
        private readonly ILogger<OrderCreateEventHandler> _logger;

        // proxy qui fait Appel a un autre service (Catalog).
        private readonly ICatalogProxy _catalogProxy;


        public OrderCreateEventHandler(
            OrderContext context,
            ICatalogProxy CatalogProxy,
            ILogger<OrderCreateEventHandler> logger)
        {
            _context = context;
            _catalogProxy = CatalogProxy;
            _logger = logger;
        }



        public async Task Handle(OrderCreateCommand notification, CancellationToken cancellationToken)
        {
            _logger.LogInformation("--- New order creation started");
            var entry = new DAL.Order();

            _logger.LogInformation("--- New order creation started");

            using (var trx = await _context.Database.BeginTransactionAsync())
            {
                // ---------Create Order-------------------
                // 01. Prepare detail
                _logger.LogInformation("--- Preparing detail");
                PrepareDetail(entry, notification);

                // 02. Prepare header
                _logger.LogInformation("--- Preparing header");
                PrepareHeader(entry, notification);

                // 03. Create order
                _logger.LogInformation("--- Creating order");
                await _context.AddAsync(entry);
                await _context.SaveChangesAsync();

                _logger.LogInformation($"--- Order {entry.OrderId} was created");
                // ---------Create Order-------------------

                //04.Update Stocks
                _logger.LogInformation("--- Updating stock");
                await _catalogProxy.UpdateStockAsync(new Models.StockUpdate()
                {
                    Items = notification.Items.Select(x => new StockUpdateItem()
                    {
                        ProductId = x.ProductId,
                        Quantity = x.Quantity,
                        Action = StockAction.Substract
                    })
                });


                await _catalogProxy.UpdateStockAsync(new Models.StockUpdate()
                {
                    Items = notification.Items.Select(x => new StockUpdateItem()
                    {
                        ProductId = x.ProductId,
                        Quantity = x.Quantity,
                        Action = StockAction.Substract
                    })
                });

                await trx.CommitAsync();
            }

            _logger.LogInformation("--- New order creation ended");
        }



        private void PrepareDetail(DAL.Order entry, OrderCreateCommand notification)
        {
            entry.Items = notification.Items.Select(x => new OrderDetail
            {
                ProductId = x.ProductId,
                Quantity = x.Quantity,
                UnitPrice = x.Price,
                Total = x.Price * x.Quantity
            }).ToList();
        }


        private void PrepareHeader(DAL.Order entry, OrderCreateCommand notification)
        {
            // Header information
            entry.Status = OrderStatus.Pending;
            entry.PaymentType = notification.PaymentType;
            entry.ClientId = notification.ClientId;
            entry.CreatedAt = DateTime.UtcNow;

            // Sum
            entry.Total = entry.Items.Sum(x => x.Total);
        }

    }
}
