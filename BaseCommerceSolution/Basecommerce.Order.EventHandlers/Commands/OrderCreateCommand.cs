﻿using Basecommerce.Order.Models;
using Basecommerce.Order.Models.Enums;
using MediatR;
using System.Collections.Generic;

namespace Basecommerce.Order.EventHandlers.Commands
{
    public class OrderCreateCommand : INotification
    {
        public OrderPayment PaymentType { get; set; }
        public int ClientId { get; set; }
        public IEnumerable<OrderCreateDetail> Items { get; set; } = new List<OrderCreateDetail>();
    }

}
