﻿using Basecommerce.WebClient.Site.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Basecommerce.WebClient.Site.Controllers
{
    public class AccountController : Controller
    {
        private readonly string _authenticationWebUrl;

        public AccountController(IConfiguration configuration)
        {
            _authenticationWebUrl = configuration.GetValue<string>("AuthenticationWebUrl");
        }


        [HttpGet]
        public IActionResult Login()
        {
            // Request.Scheme, Request.Host ==> Capture l'url de notre projet.
            // laba : https://localhost:44392/
            // ici : https://localhost:44373/
            return Redirect(_authenticationWebUrl + $"?ReturnBaseUrl={Request.Scheme}://{Request.Host}/");
        }



        [HttpGet]
        public async Task<IActionResult> Connect(string access_token)
        {
            // Notre token est divise en 3 parties par un Point.
            string[] token = access_token.Split('.');
            string value = token[1];
            value = value.PadRight(value.Length + 4 - value.Length % 4, '=');
            var resut = Convert.FromBase64String(value);

            TokenUserClaimResponse user = JsonSerializer.Deserialize<TokenUserClaimResponse>(resut);

            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.nameid),
                new Claim(ClaimTypes.Name, user.unique_name),
                new Claim(ClaimTypes.Email, user.email),
                new Claim("access_token", access_token) // ceci est utlise par apres dans getaway.web.proxy 
            };

            // connection par cookie.
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(
                claims, CookieAuthenticationDefaults.AuthenticationScheme);

            // cette cookie dure 10heures.
            AuthenticationProperties authProperties = new AuthenticationProperties
            {
                // doit durer moins que celui du token.
                IssuedUtc = DateTime.UtcNow.AddHours(10)
            };

            //connection
            await HttpContext.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsIdentity),
                authProperties);

            return Redirect("~/");
        }



        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return Redirect("~/");
        }

    }
}